library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

package numeric is
    type INTEGER_RANGE is record
        high    : integer;
        low     : integer;
        length  : integer;
    end record;

    type NATURAL_RANGE is record
        high    : natural;
        low     : natural;
        length  : natural;
     end record;

    -- invalid values, defined so that we can test which values have been    --
    -- explicitly set in record initialization functions                     --
    constant INTEGER_INVALID : integer := integer'high;
    constant INTEGER_RANGE_INVALID : INTEGER_RANGE := (others => INTEGER_INVALID);
    function F_INTEGER_RANGE(high : integer; low : integer)
        return INTEGER_RANGE;
    function F_INTEGER_RANGE(length : integer)
        return INTEGER_RANGE;
    function "=" (l : INTEGER_RANGE; r : INTEGER_RANGE)
        return boolean;
    function "/=" (l : INTEGER_RANGE; r : INTEGER_RANGE)
        return boolean;

    constant NATURAL_INVALID : natural := natural'high;
    constant NATURAL_RANGE_INVALID : NATURAL_RANGE := (others => NATURAL_INVALID);
    function F_NATURAL_RANGE(high : natural; low : natural)
        return NATURAL_RANGE;
    function F_NATURAL_RANGE(length : natural)
        return NATURAL_RANGE;
    function "=" (l : NATURAL_RANGE; r : NATURAL_RANGE)
        return boolean;
    function "/=" (l : NATURAL_RANGE; r : NATURAL_RANGE)
        return boolean;

    function to_length(value : natural)
        return natural;

    function to_high(value : natural)
        return natural;

    function to_std_logic(value : boolean)
        return std_logic;

end package;
package body numeric is
    function F_INTEGER_RANGE(high : integer; low : integer)
        return INTEGER_RANGE is
        variable o : INTEGER_RANGE := (high => high, low => low, length => high-low+1);
    begin
        return o;
    end function;
    function F_INTEGER_RANGE(length : integer)
        return INTEGER_RANGE is
    begin
        return F_INTEGER_RANGE(length-1, 0);
    end function;
    function "=" (l : INTEGER_RANGE; r : INTEGER_RANGE)
        return boolean is
    begin
        return l.high = r.high and l.low = r.low and l.length = r.length;
    end function;
    function "/=" (l : INTEGER_RANGE; r : INTEGER_RANGE)
        return boolean is
    begin
        return not "=" (l, r);
    end function;

    function F_NATURAL_RANGE(high : natural; low : natural)
        return NATURAL_RANGE is
        variable o : NATURAL_RANGE := (high => high, low => low, length => high-low+1);
    begin
        return o;
    end function;
    function F_NATURAL_RANGE(length : natural)
        return NATURAL_RANGE is
    begin
        return F_NATURAL_RANGE(length-1, 0);
    end function;
    function "=" (l : NATURAL_RANGE; r : NATURAL_RANGE)
        return boolean is
    begin
        return l.high = r.high and l.low = r.low and l.length = r.length;
    end function;
    function "/=" (l : NATURAL_RANGE; r : NATURAL_RANGE)
        return boolean is
    begin
        return not "=" (l, r);
    end function;

    function to_length(value : natural)
        return natural is
    begin
        return natural(ceil(log2(real(value+1))));
    end function;

    function to_high(value : natural)
        return natural is
    begin
        return to_length(value)-1;
    end function;

    function to_std_logic(value : boolean)
        return std_logic is
    begin
        -- assumes active-high logic
        if value then return '1'; else return '0'; end if;
    end function;
end package body;
