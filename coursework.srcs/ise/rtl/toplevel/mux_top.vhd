library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.numeric.all;
use work.nexys4.all;
use work.mux_cfg.all;

entity mux_top is
    port(
        sw          : in   std_logic_vector(15 downto 0);
        led         : out  std_logic_vector(15 downto 0)
    );
begin
end entity;

architecture mux_top_rtl of mux_top is
    constant mux_cfg : T_mux_cfg := F_mux_cfg((
        i_sel => f_natural_range(1, 0),
        i_data => f_natural_range(7, 0),
        numGroups => natural_invalid, others => natural_range_invalid
    ));
begin
    -- RTL UUT
    mux_uut:
    entity work.mux(mux_rtl)
        generic map (
            mux_cfg => mux_cfg
        )
        port map (
            i_sel   => sw(
                mux_cfg.i_data.length + mux_cfg.i_sel.high downto
                mux_cfg.i_data.length + mux_cfg.i_sel.low
                ),
            i_data  => sw(
                mux_cfg.i_data.high downto mux_cfg.i_data.low
                ),
            o_data  => led(mux_cfg.o_data.high downto mux_cfg.o_data.low)
        );
end architecture;
