library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.numeric.all;

package mux_cfg is
    type T_mux_cfg is record
        i_sel      : natural_range;
        i_data     : natural_range;

        o_data     : natural_range;

        numGroups : natural;
    end record;

    function F_mux_cfg(i : T_mux_cfg) return T_mux_cfg;

end package;
package body mux_cfg is

    function F_mux_cfg(i : T_mux_cfg) return T_mux_cfg is
        variable o : T_mux_cfg := i;
    begin
        if  i.i_data    /= natural_range_invalid and
            i.o_data    /= natural_range_invalid
        then
            o.i_numGroups := i.i_data.length / i.o_data.length;
            o.i_sel := F_natural_range(to_length(o.i_numGroups));
        end if;

        if  i.i_sel     /= natural_range_invalid and
            i.i_data    /= natural_range_invalid
        then
            o.numGroups := 2**(o.i_sel.length);
            o.o_data := F_natural_range((o.i_data.length) / (o.numGroups));
        end if;

        if o.i_sel      = natural_range_invalid then report "i_sel not defined" severity error; end if;
        if o.i_data     = natural_range_invalid then report "i_data not defined" severity error; end if;
        if o.o_data     = natural_range_invalid then report "o_data not defined" severity error; end if;
        if numGroups    = natural_invalid then report "numGroups not defined" severity error; end if;
        return o;
    end function;
end package body;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.mux_cfg.all;

entity mux is
    generic (
        mux_cfg : T_mux_cfg
    );
    port (
        i_sel     : in  std_logic_vector(mux_cfg.i_sel.high downto mux_cfg.i_sel.low);
        i_data    : in  std_logic_vector(mux_cfg.i_data.high downto mux_cfg.i_data.low);

        o_data    : out std_logic_vector(mux_cfg.o_data.high downto mux_cfg.o_data.low)
    );
end entity;

architecture mux_rtl of mux is
    type T_i is record
        sel     : std_logic_vector(i_sel'range);
        data    : std_logic_vector(i_data'range);
    end record;
    signal i : T_i;

    type T_o is record
        data    : std_logic_vector(o_data'range);
    end record;
    signal o : T_o;

    type T_muxArray is array (mux_cfg.numGroups-1 downto 0)
        of std_logic_vector(mux_cfg.o_data.high downto 0);
    signal muxArray    : T_muxArray;
begin
    -- signal to record remap
    i.sel     <= i_sel;
    i.data    <= i_data;

    o_data    <= o.data;

    -- input data vector to array remap
    G_muxAssign:
    for v_curGroup in 0 to mux_cfg.numGroups-1 generate
    begin
        muxArray(v_curGroup) <= i.data(
            ((v_curGroup+1) * mux_cfg.o_data.length)-1 downto v_curGroup * mux_cfg.o_data.length
        );
    end generate;
    -- pure combinatoric process
    P_comb:
    process(i, muxArray) is                                                     -- even though muxArray is driven by i_data, if we only define i_data, the simulator
    begin                                                                       -- will treat muxArray as registered (with an implicit clock)
        o.data <= muxArray(to_integer(unsigned(i.sel)));
    end process;
end architecture;

