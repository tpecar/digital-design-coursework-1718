library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package nexys4 is
    type T_nexys4 is record
        clk         : std_logic;
        sw          : std_logic_vector(15 downto 0);
        led         : std_logic_vector(15 downto 0);
        RGB1_Red    : std_logic;
        RGB1_Green  : std_logic;
        RGB1_Blue   : std_logic;
        RGB2_Red    : std_logic;
        RGB2_Green  : std_logic;
        RGB2_Blue   : std_logic;
        seg         : std_logic_vector(6 downto 0);
        dp          : std_logic;
        an          : std_logic_vector(7 downto 0);
        btnCpuReset : std_logic;
        btnC        : std_logic;
        btnU        : std_logic;
        btnL        : std_logic;
        btnR        : std_logic;
        btnD        : std_logic;
        vgaRed      : std_logic_vector(3 downto 0);
        vgaBlue     : std_logic_vector(3 downto 0);
        vgaGreen    : std_logic_vector(3 downto 0);
        Hsync       : std_logic;
        Vsync       : std_logic;
    end record;

    constant nexys4_init : T_nexys4 := (
        clk         => '0',
        sw          => (others => '0'),
        led         => (others => '0'),
        RGB1_Red    => '1',
        RGB1_Green  => '1',
        RGB1_Blue   => '1',
        RGB2_Red    => '1',
        RGB2_Green  => '1',
        RGB2_Blue   => '1',
        seg         => (others => '1'),
        dp          => '1',
        an          => (others => '1'),
        btnCpuReset => '1',
        btnC        => '0',
        btnU        => '0',
        btnL        => '0',
        btnR        => '0',
        btnD        => '0',
        vgaRed      => (others => '0'),
        vgaBlue     => (others => '0'),
        vgaGreen    => (others => '0'),
        Hsync       => '0',
        Vsync       => '0'
    );

end package;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library work;
use work.nexys4.all;

entity nexys4_top is
    port (
        clk         : in   std_logic;
        sw          : in   std_logic_vector(15 downto 0);
        led         : out  std_logic_vector(15 downto 0);
        RGB1_Red    : out  std_logic;
        RGB1_Green  : out  std_logic;
        RGB1_Blue   : out  std_logic;
        RGB2_Red    : out  std_logic;
        RGB2_Green  : out  std_logic;
        RGB2_Blue   : out  std_logic;
        seg         : out  std_logic_vector(6 downto 0);
        dp          : out  std_logic;
        an          : out  std_logic_vector(7 downto 0);
        btnCpuReset : in   std_logic;
        btnC        : in   std_logic;
        btnU        : in   std_logic;
        btnL        : in   std_logic;
        btnR        : in   std_logic;
        btnD        : in   std_logic;
        vgaRed      : out  std_logic_vector(3 downto 0);
        vgaBlue     : out  std_logic_vector(3 downto 0);
        vgaGreen    : out  std_logic_vector(3 downto 0);
        Hsync       : out  std_logic;
        Vsync       : out  std_logic;

        ports       : inout T_nexys4
    );
begin
end entity;

architecture nexys4_top_rtl of nexys4_top is
begin
    -- actual drivers
    ports.clk         <=        clk;
    ports.sw          <=        sw;
          led         <=  ports.led;
          RGB1_Red    <=  ports.RGB1_Red;
          RGB1_Green  <=  ports.RGB1_Green;
          RGB1_Blue   <=  ports.RGB1_Blue;
          RGB2_Red    <=  ports.RGB2_Red;
          RGB2_Green  <=  ports.RGB2_Green;
          RGB2_Blue   <=  ports.RGB2_Blue;
          seg         <=  ports.seg;
          dp          <=  ports.dp;
          an          <=  ports.an;
    ports.btnCpuReset <=        btnCpuReset;
    ports.btnC        <=        btnC;
    ports.btnU        <=        btnU;
    ports.btnL        <=        btnL;
    ports.btnR        <=        btnR;
    ports.btnD        <=        btnD;
          vgaRed      <=  ports.vgaRed;
          vgaBlue     <=  ports.vgaBlue;
          vgaGreen    <=  ports.vgaGreen;
          Hsync       <=  ports.Hsync;
          Vsync       <=  ports.Vsync;
end architecture;
