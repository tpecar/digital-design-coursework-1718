library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.numeric.all;

package seg7_cfg is
    type T_seg7_cfg is record
        i_data  : natural_range;

        o_an    : natural_range;
    end record;
end package;
package body seg7_cfg is
    function F_seg7_cfg(i : T_seg7_cfg) return T_seg7_cfg is
        variable o : T_seg7_cfg := i;
    begin
        if i.i_data = natural_range_invalid then report "i_data not defined" severity error; end if;
        if i.i_data mod 4 /= 0 then report "i_data has to have 4*n bits" severity error; end if;

        if i.o_an   = natural_range_invalid then o.o_an = F_natural_range(i_data / 4); end if;
    end function;
end package body;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.numeric.all;
use work.comb.all;
use work.seg7_cfg.all;
use work.mux_cfg.all;

entity seg7 is
    generic (
        seg7_cfg : T_seg7_cfg
    );
    port (
        i_clk   : std_logic;
        i_rst   : std_logic;

        i_data  : std_logic_vector(seg7_cfg.i_data.high downto seg7_cfg.i_data.low);

        o_seg   : std_logic_vector(6 downto 0);                                 -- dp is ignored
        o_an    : std_logic_vector(seg7_cfg.o_an.high downto seg7_cfg.o_an.low);
    );
begin
end entity;

architecture seg7_rtl of seg7 is
    -- configuration
    mux_cfg : T_mux_cfg := F_mux_cfg(
        (
        i_data  => seg7_cfg.i_data,
        o_data  => natural_range(4),
        -- other parameters are calculated
        numGroups => natural_invalid,
        others  => natural_range_invalid
        )
    );

    -- input
    type T_i is record
        data    : std_logic_vector(i_data'range);
    end record;
    signal i : T_i;

    -- output
    type T_o is record
        seg   : std_logic_vector(o_seg'range);
        an    : std_logic_vector(o_an'range);
    end record;
    signal o : T_o;

    -- registers (state)
    type T_r is record
        anSelect : std_logic_vector(o_an'range);
        digitDisplayPrescaler : T_prescaler;
    end record;
    variable    rc      : T_r; -- next state (combinatoric), before assigned to rin
    signal      r, rin  : T_r; -- r - current state, rin - next state
begin
    -- signal to record map
    i.data  <= i_data;

    o_seg   <= o.seg;
    o_an    <= o.an;

    -- component instantiation
    entity work.mux(mux_rtl)
        generic map(mux_cfg)
        port map (
            i_sel =>
        );

    -- combinatoric process
    P_comb:
    process(i_rst, i, r) is
    begin
        -- if reset asserted, set registers to initial state
        if rst_i = '1' then
            rc.anSelect := (0 => '1', others => '0');
            rc.digitDisplayPrescaler := (0, '0');
        else
            -- shift anode selection
            -- encode to mux select and select data to be displayed
            -- encode data bits to segments
        end if;

        -- assign next register state
        rin <= rc;
    end process;

    -- sequential process
    P_seq:
    process(i_clk) is
    begin
        if rising_edge(i_clk) then r <= rin; end if;
    end process;
end architecture;
