-- common combinatorics
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.numeric.all;

package comb is
    -- counter
    function F_counter(curVal : unsigned; max : integer)
        return unsigned;
    -- prescaler
    type T_prescaler is record
        curVal  : unsigned;
        enable  : std_logic;
    end record;
    function F_prescaler(curVal : unsigned; max : integer)
        return T_prescaler;
    -- decoder
    function F_decoder(i : std_logic_vector)
        return std_logic_vector;
    -- encoder
    function F_encoder(i : std_logic_vector)
        return std_logic_vector;
end package;
package body comb is
    -- counter
    function F_counter(curVal : unsigned; max : integer)
        return unsigned is
    begin
        if curVal = to_unsigned(max, curVal'length) then return to_unsigned(0, curVal'length);
        else return curVal + 1; end if;
    end function;
    -- prescaler
    function F_prescaler(curVal : unsigned; max : integer)
        return T_prescaler is
    begin
        if curVal = to_unsigned(max, curVal'length) then
            return (to_unsigned(0, curVal'length), '1');
        else
            return (curVal + 1, '0');
        end if;
    end function;
    -- decoder
    function F_decoder(i : std_logic_vector)
        return std_logic_vector is
        variable o : std_logic_vector(2**i'length downto 0);
    begin
        for val in 0 to 2**i'length-1 loop
            o(val) := to_std_logic(std_logic_vector(to_unsigned(val, i'length)) = i);
        end loop;
    end function;
    -- encoder
    function F_encoder(i : std_logic_vector)
        return std_logic_vector is
    begin
        -- lsb priority encoder
        for val in 0 to i'length loop
            if i(val) = '1' then
                return std_logic_vector(to_unsigned(val, to_length(i'length)));
            end if;
        end loop;
    end function;
end package body;
