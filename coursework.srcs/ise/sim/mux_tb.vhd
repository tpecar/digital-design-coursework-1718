library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.mux_cfg.all;
use work.numeric.all;

entity mux_tb is
   -- very simple counter combinatoric
   function F_counter(vi_curVal : unsigned; vi_max : integer)
      return unsigned is
   begin
      if vi_curVal = to_unsigned(vi_max, vi_curVal'length) then return to_unsigned(0, vi_curVal'length);
      else return vi_curVal + 1; end if;
   end function;
   
end mux_tb;

architecture mux_tb_sim of mux_tb is
   signal clk : std_logic := '1';
   
   signal counter : unsigned(3 downto 0) := "0000";
   
   -- UUT configuration
   constant mux_cfg : T_mux_cfg := F_mux_cfg(
      (i_sel => f_natural_range(1, 0), i_data => f_natural_range(3, 0), numGroups => natural_invalid, others => natural_range_invalid)
   );
   signal muxSel : std_logic_vector(mux_cfg.i_sel.high downto mux_cfg.i_sel.low) := std_logic_vector(to_unsigned(1, mux_cfg.i_sel.length));
   signal muxOut : std_logic_vector(mux_cfg.o_data.high downto mux_cfg.o_data.low);
   
begin
   P_clk:
   process is
   begin
      clk <= not clk;
      wait for 10 us;
   end process;

   P_count:
   process(clk) is
   begin
      if rising_edge(clk) then
         counter <= F_counter(counter, 3);
      end if;
   end process;

   -- UUT
   mux:
   entity work.mux(mux_rtl)
      generic map (mux_cfg)
      port map (
         i_sel => muxSel,
         i_data => std_logic_vector(counter),
         
         o_data => muxOut
      );
end mux_tb_sim;

