-----------------------------------------------------------------------------------------------
-- The coding convention used is based on the
--  [1] Design and VHDL handbook for VLSI development - CNES Edition
--      available @ https://github.com/VHDLTool/VHDL_Handbook_CNE/releases
--
-- and in cases where the aforementioned specification misses rules, we ammend it with
--  [2] ALSE's VHDL Design Rules & Coding Style
--      available @ https://wiki.electroniciens.cnrs.fr/images/VHDL_Coding_eng.pdf
--
-- Information that might be ambiguous or missing from the above lists is mentioned inline
-- using --##
-----------------------------------------------------------------------------------------------
--
-- Generalized common logic modules
--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library common;
use common.Common.ALL;

entity jellybean_mux is
    generic(
        g_InWidth   : natural;
        g_OutWidth  : natural
    );
    port (
        i_InVec     : in std_logic_vector(g_InWidth-1 downto 0);
        -- try a different methodology
        -- instead of inferring the vector sizes etc. from generic parameters, make
        -- EVERYTHING explicit
        -- this way we will avoid stupid vector calculation errors
        --
        -- but instead of having a bunch of parameters, pack them in a record
        -- provide a package that has methods that calculate the record values
        -- based on parameters
        --
        -- this way, you can calculate the values and use the record
        -- values for manually setting up other records
        --
        -- do note that you can also bunch your signals in a record and it
        -- will be synthesizable
        -- however, in a generic this could only be used to provide initial
        -- values for simulation - records are not overloadable
        --
        i_Select    : in std_logic_vector(
                Common_F_GetWidth((g_InWidth / g_OutWidth)-1)-1 downto 0        -- (g_InWidth / g_OutWidth)-1 because 0 is one of the states
            );

        o_OutVec    : out std_logic_vector(g_OutWidth-1 downto 0)
    );
begin
end entity;

architecture jellybean_mux_rtl of jellybean_mux is
    constant c_NumSelections    : natural := g_InWidth / g_OutWidth;                -- number of bit groups the InVec gets split into

    type T_MuxVec is array (natural range <>) of std_logic_vector(g_OutWidth-1 downto 0);
    signal MuxVec : T_MuxVec(c_NumSelections-1 downto 0);
begin
--    -- selects a 4-bit pair from the given vector
--    -- for the purposes of selecting a value for digit display
--    -- NON-SYNTHESIZABLE: [Synth 8-561] range expression could not be resolved to a constant
--    HexDigit <= i_Vec(
--                        -- the seemingly unnecessary coversion to bit_vector and back again is done
--                        -- so that we convert the initial (at the start of simulation) 'U..U' state of SelectedAnodeIdx to '0..0'
--                        -- otherwise the unsigned() function treats a vector with U bits as invalid and returns an invalid
--                        -- value, which upsets range calculation and causes the simulator to abort the simulation
--                        to_integer(unsigned(to_stdlogicvector(to_bitvector(SelectedAnodeIdx)) & std_logic_vector'("11")))
--                        downto
--                        to_integer(unsigned(to_stdlogicvector(to_bitvector(SelectedAnodeIdx)) & std_logic_vector'("00")))
--                    );
    --#
    GEN_MuxVecAssign:
    for v_CurSelection in 0 to c_NumSelections-1 generate
    begin
        MuxVec(v_CurSelection) <= i_InVec(
            ((v_CurSelection+1) * g_OutWidth)-1                                 -- high index of a 4-bit group (+1 to account for v_CurSelection = 0)
            downto
            v_CurSelection * g_OutWidth                                         -- low index of a 4-bit group
        );
    end generate GEN_MuxVecAssign;

    o_OutVec <= MuxVec(to_integer(unsigned(i_Select)));
end architecture;

-----------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library common;
use common.Common.ALL;

entity jellybean_decoder is
    generic(
        g_InWidth : natural
    );
    port(
        i_InVec     : in std_logic_vector(g_InWidth-1 downto 0);

        o_OutVec    : out std_logic_vector((2**g_InWidth)-1 downto 0) := (others => '0')
    );
begin
end entity;

architecture jellybean_decoder_rtl of jellybean_decoder is
begin
--    -- one-hot encoding, for the purposes of anode select
--    -- NON-SYNTHESIZABLE: [Synth 8-211] could not evaluate expression: aggregate choice expression
--    o_SelectedAnodeOneHot <= (to_integer(unsigned(SelectedAnodeIdx)) => '1', others => '0');

    P_SelectIdx:
    process(i_InVec) is
    begin
        -- synthesizes into logic
        -- see https://stackoverflow.com/questions/26880270/for-generate-inside-process-vhdl
        -- for details
        for v_CurInVecVal in 0 to (2**g_InWidth)-1 loop
            if v_CurInVecVal = to_integer(unsigned(i_InVec)) then
                o_OutVec(v_CurInVecVal) <= '1';
            else
                o_OutVec(v_CurInVecVal) <= '0';
            end if;
        end loop;
    end process;
end architecture;
