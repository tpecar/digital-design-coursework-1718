-----------------------------------------------------------------------------------------------
-- The coding convention used is based on the
--  [1] Design and VHDL handbook for VLSI development - CNES Edition
--      available @ https://github.com/VHDLTool/VHDL_Handbook_CNE/releases
--
-- and in cases where the aforementioned specification misses rules, we ammend it with
--  [2] ALSE's VHDL Design Rules & Coding Style
--      available @ https://wiki.electroniciens.cnrs.fr/images/VHDL_Coding_eng.pdf
--
-- Information that might be ambiguous or missing from the above lists is mentioned inline
-- using --##
-----------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library common;
use common.Common.ALL;

package timing_counter_cfg is
    type T_timing_counter_cfg is record
        MaxValue            : natural;
        CounterValue_length : natural;
    end record;

    function F_timing_counter_cfg(
            MaxValue :              natural   := natural_invalid;
            CounterValue_length :   natural   := natural_invalid
        )
        return T_timing_counter_cfg;
end package;
package body timing_counter_cfg is
    function F_timing_counter_cfg(
            MaxValue :              natural   := natural_invalid;
            CounterValue_length :   natural   := natural_invalid
        )
        return T_timing_counter_cfg is
        variable o : T_timing_counter_cfg := (MaxValue, CounterValue_length);
    begin
        if MaxValue = natural_invalid and CounterValue_length /= natural_invalid then
            o.MaxValue := 2**CounterValue_length-1;
        end if;
        if CounterValue_length = natural_invalid then
            o.CounterValue_length := Common_F_GetWidth(MaxValue);
        end if;
        return o;
    end function;
end package body;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library common;
use common.Common.ALL;
use common.timing_counter_cfg.ALL;

--# name of an entity should be derived from the filename
entity timing_counter is
    generic(
        timing_counter_cfg : T_timing_counter_cfg
    );
    port (
        i_Clk           : in    std_logic;
        i_Rst           : in    std_logic;
        i_Enable        : in    std_logic;

        o_CounterValue  : out   std_logic_vector
            (timing_counter_cfg.CounterValue_length-1 downto 0);
        o_MaxAchieved   : out   std_logic
    );
begin
end entity;

--## synthesizable architectures get a _rtl postix
architecture timing_counter_rtl of timing_counter is
    --## signal names are without prefixes
    signal CounterValue : unsigned(timing_counter_cfg.CounterValue_length-1 downto 0);
begin
    o_CounterValue <= std_logic_vector(CounterValue);

    P_ResetCounter_AfterMaxValueAchieved:                                       -- Resets counter when it counts up to g_MaxValue.
    process (i_Clk) is
    begin
        if rising_edge(i_Clk) then
            o_MaxAchieved <= '0';                                               -->default driving value if no other statement assigns to it in the same
                                                                                -- clock cycle
            if i_Rst = '1' then                                                 --  NOTE that the "[Synth 8-6014] Unused sequential element x was removed." warning is
                CounterValue <= (others => '0');                                --  actually a synthesis tool bug that is present in 2017 versions of Vivado.
                                                                                --  The design itself is nevertheless correct.
            elsif i_Enable = '1' then                                           -- See:
                if CounterValue = timing_counter_cfg.MaxValue then              --      https://forums.xilinx.com/t5/Synthesis/Unused-sequential-element-was-removed-no-idea-why/td-p/801128
                    o_MaxAchieved <= '1';                                       --      https://forums.xilinx.com/t5/Synthesis/Many-spurious-quot-Synth-8-6014-Unused-sequential-element-was/td-p/769636/page/2
                    CounterValue <= (others => '0');                            -- for more info

                else
                    CounterValue <= CounterValue + to_unsigned(1, timing_counter_cfg.CounterValue_length);

                end if;
            end if;
        end if;
    end process;
end architecture;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

library common;
use common.Common.ALL;

package timing_prescaler_cfg is
    type T_timing_prescaler_cfg is record
        PrescaleFactor : natural;
    end record;

    function F_timing_prescaler_cfg(
            PrescaleFactor : natural := natural_invalid
        )
        return T_timing_prescaler_cfg;

    -- time -> ticks calculation --
    function F_GetTicks_Time(vi_ClkFreq : real;                                 -- number of ticks required for the provided clock to achieve the
        vi_TargetTime : real)                                                   -- required time delay (in s)
        return natural;

    function F_GetTicks_Freq(vi_ClkFreq : real;                                 -- number of ticks required for the provided clock to achieve the
        vi_TargetFreq : real)                                                   -- required frequency (in Hz)
        return natural;
end package;

package body timing_prescaler_cfg is
    function F_timing_prescaler_cfg(
            PrescaleFactor : natural := natural_invalid
        )
        return T_timing_prescaler_cfg is
        variable o : T_timing_prescaler_cfg := (others => PrescaleFactor);      -- single element records have to be initalized with others, otherwise
                                                                                -- the expression gets reduced to a value istead of being a record
    begin
        if PrescaleFactor = natural_invalid then
            report "PrescaleFactor invalid";
        end if;
        return o;
    end function;

    -- time -> ticks calculation --
    function F_GetTicks_Time(vi_ClkFreq : real; vi_TargetTime : real)
        return natural is
        variable v_Ticks : natural;
    begin
        v_Ticks := natural(ceil(vi_ClkFreq / (1.0 / vi_TargetTime)));
        return v_Ticks;
    end function;

    function F_GetTicks_Freq(vi_ClkFreq : real; vi_TargetFreq : real)
        return natural is
        variable v_Ticks : natural;
    begin
        v_Ticks := natural(ceil(vi_ClkFreq / vi_TargetFreq));
        return v_Ticks;
    end function;
end package body;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library common;
use common.Common.ALL;
use common.timing_counter_cfg.ALL;
use common.timing_prescaler_cfg.ALL;

entity timing_prescaler is
    generic(
        timing_prescaler_cfg : T_timing_prescaler_cfg
    );
    port (
        i_Clk       : in  std_logic;
        i_Rst       : in  std_logic;

        o_Enable    : out std_logic
    );
begin
end entity;

-- When implementing using a "traditional" counter with no max value checking:
-- While on first look it might seems like a good idea to use a counter component to implement the prescaler,
-- its implementation is problematic because
--  - the counter entity has a synchronous reset - we have to hold the reset signal for the whole clock cycle
--    in order for it to get registered on the next clock cycle
--  - the counter will count an additional value before it will be reset
--    (which makes it inefficient, even if we use an asynchronous reset)
--  - synthesis tools treat entities separately, so by separating functionality you might be losing some
--    of the optimizations done
--  With that, we introduce an additional wait state, which in general could be compensated by counting up to
--  prescaler factor -1 (to compensate for the wait state) -1 (to compensate for counting from 0).
--    This design does not work if we want to divide by 2, since we would detect at 0, therefore we would always
--    be in reset/enable.
--
-- Its easier to just reimplement the counter and give it a detection capability which will reset the counter
-- in the same cycle.
--
-- So, be vary of applying software principles to hardware design.
--


-- The above commentary is for the case when you have a dumb counter with no
-- maximum value checks.
-- By implementing the counter with bounds checking (and an indication of when
-- the maximum value was achieved), one can repurpose the counter as a
-- prescaler.
--
-- One has to note that a prescaler has to count up to, but not including,
-- prescaler factor, since we have 0 as one of the states that takes 1 clock
-- cycle.
architecture timing_prescaler_rtl of timing_prescaler is
begin
    Counter:
    entity common.timing_counter(timing_counter_rtl)
        generic map(
            timing_counter_cfg =>
                F_timing_counter_cfg(
                    MaxValue => timing_prescaler_cfg.PrescaleFactor-1           -- -1 because 0 is one of the states
                )
        )
        port map(
            i_Clk => i_Clk,
            i_Rst => i_Rst,
            i_Enable => '1',

            o_CounterValue => open,
            o_MaxAchieved => o_Enable
        );
end architecture;
