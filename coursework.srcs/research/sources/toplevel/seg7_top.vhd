 -----------------------------------------------------------------------------------------------
-- The coding convention used is based on the
--  [1] Design and VHDL handbook for VLSI development - CNES Edition
--      available @ https://github.com/VHDLTool/VHDL_Handbook_CNE/releases
--
-- and in cases where the aforementioned specification misses rules, we ammend it with
--  [2] ALSE's VHDL Design Rules & Coding Style
--      available @ https://wiki.electroniciens.cnrs.fr/images/VHDL_Coding_eng.pdf
--
-- Information that might be ambiguous or missing from the above lists is mentioned inline
-- using --##
-----------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library common;
use Common.common.ALL;

-- entity for testing hex controller
entity seg7_toplevel is
    port(
        -- clock
        CLK100MHZ : in std_logic;
        -- reset
        CPU_RESETN : in std_logic;                                              -- negative logic

        -- buttons
        SW : in std_logic_vector(15 downto 0);

        -- 7segment display
        CA : out std_logic;
        CB : out std_logic;
        CC : out std_logic;
        CD : out std_logic;
        CE : out std_logic;
        CF : out std_logic;
        CG : out std_logic;
        DP : out std_logic;
        AN : out std_logic_vector(7 downto 0)
    );
begin
end entity;

architecture seg7_toplevel_rtl of seg7_toplevel is
    -- signals for remapping
    signal Clk : std_logic;
    signal Rst : std_logic;

    signal CathodeSelect : std_logic_vector(7 downto 0);
    signal AnodeSelect : std_logic_vector(7 downto 0);
begin
    -- remap signals
    Clk <= CLK100MHZ;
    Rst <= not CPU_RESETN;

    CA <= CathodeSelect(0);
    CB <= CathodeSelect(1);
    CC <= CathodeSelect(2);
    CD <= CathodeSelect(3);
    CE <= CathodeSelect(4);
    CF <= CathodeSelect(5);
    CG <= CathodeSelect(6);
    DP <= CathodeSelect(7);

    AN <= AnodeSelect;

    -- display controller
    Seg7Controller:
    entity common.seg7_hexController(seg7_hexController_rtl)
        generic map(
            g_VecWidth      => 16,
            g_ClkFreq       => 100.0e+6,
            g_RefreshRate   => 30.0
        )
        port map(
            i_Clk => Clk,
            i_Rst => Rst,

            i_Vec => SW,

            o_AnodeEnable => AnodeSelect(3 downto 0),
            o_CathodeEnable => CathodeSelect
        );
    -- disable upper digits
    AnodeSelect(7 downto 4) <= (others => '1');
end architecture;
