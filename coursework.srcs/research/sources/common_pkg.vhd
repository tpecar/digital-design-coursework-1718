-----------------------------------------------------------------------------------------------
-- Contains common functions and constants.
-- Since the secondary VHDL unit here is the package body, the library usage definitions
-- are _NOT_ passed onto entities.
--
-- See the following links for more detail:
--  http://insights.sigasi.com/tech/use-and-library-vhdl.html
--  http://insights.sigasi.com/tech/scope-vhdl-use-clauses-and-vhdl-library-clauses.html
-----------------------------------------------------------------------------------------------
-- The library into which the entites get compiled, is specified by the .prj file that is
-- provided to the xvhdl command.
--
-- This package is meant to reside in the "common" library.
-----------------------------------------------------------------------------------------------
-- The coding convention used is based on the
--  [1] Design and VHDL handbook for VLSI development - CNES Edition
--      available @ https://github.com/VHDLTool/VHDL_Handbook_CNE/releases
--
-- and in cases where the aforementioned specification misses rules, we ammend it with
--  [2] ALSE's VHDL Design Rules & Coding Style
--      available @ https://wiki.electroniciens.cnrs.fr/images/VHDL_Coding_eng.pdf
--
-- Information that might be ambiguous or missing from the above lists is mentioned inline
-- using --##
-----------------------------------------------------------------------------------------------
-- One should avoid using STD_LOGIC_ARITH, STD_LOGIC_UNSIGNED libraries for arithmetic
-- operations, since they are non-standard and introduce error-prone implicit vector + integer
-- functions.
--
-- Any arithmetic should be done using signed/unsigned types, which is provided by the
-- IEEE.NUMERIC_STD library.
--
-- For more info see:
--  http://fpgasite.blogspot.si/2017/04/signed-unsigned-and-stdlogicvector.html
--  http://vhdlguru.blogspot.si/2010/03/why-library-numericstd-is-preferred.html
--  https://forums.xilinx.com/t5/Synthesis/IEEE-NUMERIC-STD-ALL/td-p/301857
-----------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use IEEE.STD_LOGIC_TEXTIO.ALL;

-- seems that the Synopsis variant is supported
-- https://www.csee.umbc.edu/portal/help/VHDL/std_logic_textio.vhdl
library STD;
use STD.TEXTIO.ALL;

package Common is
    type T_SomeRecord is record
        test        : integer;
        test2       : integer;
    end record;

    function F_SomeRecord(
        test    : integer := -1;
        test2   : integer := -1
    )
        return T_SomeRecord;

    constant SomeRecordVal : T_SomeRecord := F_SomeRecord(
            test => 1
        );

    -- invalid values, defined so that we can test which values have been   --
    -- explicitly set in record initialization functions                    --
    constant INTEGER_INVALID : integer := integer'high;
    constant NATURAL_INVALID : natural := natural'high;

    -- argument calculation --
    function Common_F_GetWidth(vi_MaxValue : natural)
        return natural;

    -- string manipulation functions --

    type T_None is (None);                                                      -- dummy void type and shared variable to catch an output of an overloaded operator function
    shared variable void : T_None;                                              --# types have the T_ prefix

    function s(vi_Str : string)                                                 -- returns string, unmodified
        return string;                                                          -- so that we can force string literals to be treated as strings
                                                                                -- (and not as bit_string/std_vector types)

    function "*" (vi_Prefix : string; vi_Value : string)                        -- string + string
        return string;                                                          --

    function "*" (vi_Prefix : string; vi_Value : std_logic_vector)              -- string + vector(binary)
        return string;                                                          --

    type T_ToHex is (TOHEX);                                                    -- string + vector(hex)
    type T_SLV_Hex is record                                                    --
        PrefixPtr : line;                                                       --
    end record;                                                                 --
    function "*" (vi_Prefix : string; vi_Keyword : T_ToHex)                     --
        return T_SLV_Hex;                                                       --
    function "*" (vi_Prefix : T_SLV_Hex; vi_Value : std_logic_vector)           --
        return string;                                                          --

    -- TODO:
    -- add
    --      string + integer
    --      string + real
    -- integer to bin/hex can be accomplished by converting them to
    -- std_logic_vector

    type T_FileOp is (STDOUT);                                                  -- writes string to provided text file, returns provided string
    impure function ">" (vi_Result : string; vi_Output : T_FileOp)              --
        return string;                                                          -- vi_Output is an enumerated type, acting as a keyword for our domain
                                                                                --   specific language
                                                                                --
                                                                                -- beware that if using a function, its return value has to be explicitly caught by a variable,
                                                                                -- otherwise the compiler confusingly complains about expecting a 'void' type in place of the
                                                                                -- last argument

    impure function ">" (vi_Result : string; vi_Output : T_FileOp)              -- writes string to provided text file, returns None (enum)
        return T_None;                                                          --

end package;

package body Common is

    function F_SomeRecord(
        test    : integer := -1;
        test2   : integer := -1
    )
        return T_SomeRecord is
        variable v_SomeRecord : T_SomeRecord;
    begin
        v_SomeRecord.test   := test;
        v_SomeRecord.test2  := test2;
        if test<0 then
            v_SomeRecord.test := test2-1;
        end if;
        if test2<0 then
            v_SomeRecord.test2 := test+1;
        end if;
        return v_SomeRecord;
    end function;

    --## omit the the pkg_ prefix, the prefix before F designates a package
    --## F_ specifies a function
    --## vi_ specifies variables in an interface list
    --## block construct names are not repeated

    -- argument calculation --
    function Common_F_GetWidth(vi_MaxValue : natural)
        return natural is
        variable v_Width : natural;
    begin
        v_Width := natural(ceil(log2(real(vi_MaxValue+1))));
        return v_Width;
    end function;

    -- string manipulation functions --
    function s (vi_Str : string)
        return string is
    begin
        return vi_Str;
    end function;

    function "*" (vi_Prefix : string; vi_Value : string)
        return string is
    begin
        return vi_Prefix & vi_Value;
    end function;

    function "*" (vi_Prefix : string; vi_Value : std_logic_vector)
        return string is
        variable v_PrefixPtr : line;
    begin
        v_PrefixPtr := new string'(vi_Prefix);
        write(v_PrefixPtr, to_bitvector(vi_Value));
        return v_PrefixPtr.all;
    end function;

    function "*" (vi_Prefix : string; vi_Keyword : T_ToHex)
        return T_SLV_Hex is
        variable v_T_SLV_Hex : T_SLV_Hex;
    begin
        v_T_SLV_Hex.PrefixPtr := new string'(vi_Prefix);
        return v_T_SLV_Hex;
    end function;

    function "*" (vi_Prefix : T_SLV_Hex; vi_Value : std_logic_vector)
        return string is
        variable v_PrefixPtr : line;
    begin
        -- you cannot directly provide a record element as an argument,
        -- so we have to copy the pointer to a standalone variable
        v_PrefixPtr := vi_Prefix.PrefixPtr;

        hwrite(v_PrefixPtr, vi_Value, RIGHT, 0);
        return v_PrefixPtr.all;
    end function;

    impure function ">" (vi_Result : string; vi_Output : T_FileOp)
        return string is
        variable v_ResultPtr : line;
    begin
        -- in VHDL we cannot explicitly get a reference to an
        -- object, so we have to instantiate a new one using
        -- the new operator
        v_ResultPtr := new string'(vi_Result);
        writeline(OUTPUT, v_ResultPtr);
        return vi_Result;
    end function;

    -- termination variant of the above
    impure function ">" (vi_Result : string; vi_Output : T_FileOp)
        return T_None is
        -- variable allocation happens at function invocation, so the following
        -- is allowed
        variable v_Result : string(vi_Result'range);
    begin
        -- dump result into a dummy variable
        v_Result := ">" (vi_Result, vi_Output);
        return None;
    end function;

end package body;
