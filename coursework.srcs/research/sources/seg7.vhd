-----------------------------------------------------------------------------------------------
-- The coding convention used is based on the
--  [1] Design and VHDL handbook for VLSI development - CNES Edition
--      available @ https://github.com/VHDLTool/VHDL_Handbook_CNE/releases
--
-- and in cases where the aforementioned specification misses rules, we ammend it with
--  [2] ALSE's VHDL Design Rules & Coding Style
--      available @ https://wiki.electroniciens.cnrs.fr/images/VHDL_Coding_eng.pdf
--
-- Information that might be ambiguous or missing from the above lists is mentioned inline
-- using --##
-----------------------------------------------------------------------------------------------


-- implementation for the configuration present on the Nexys boards:
--  - common anode (one anode per segment)
--      anode enabled   @ 0 (due to transistors)
--  - per-segment cathodes
--      cathode enabled @ 0 (pull towards GND)
-- assuming the following indexing of segments
--          [0]
--       ### A ###
--       #       #
--   [5] F       B [1]
--       #  [6]  #
--       ### G ###
--       #       #
--   [4] E       C [2]
--       #       #     [7]
--       ### D ###  ## DP
--          [3]
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

--# multi-word entity names are camel-cased
entity seg7_encoderHex is
    port (
        i_HexDigit  : in std_logic_vector(3 downto 0);                          -- asynchronous operation

        o_CathodeEnable : out std_logic_vector(7 downto 0)                      -- assuming 8 segment display (7 digit segments + dot)
    );

    constant A  : std_logic_vector(0 to 7) := "01111111";                       -- note the "to" - LSB to MSB notation
    constant B  : std_logic_vector(0 to 7) := "10111111";
    constant C  : std_logic_vector(0 to 7) := "11011111";
    constant D  : std_logic_vector(0 to 7) := "11101111";
    constant E  : std_logic_vector(0 to 7) := "11110111";
    constant F  : std_logic_vector(0 to 7) := "11111011";
    constant G  : std_logic_vector(0 to 7) := "11111101";
    constant DP : std_logic_vector(0 to 7) := "11111110";

    type T_SegmentData is array(0 to 15) of std_logic_vector(0 to 7);
    constant c_SegmentData : T_SegmentData := (
        A and B and C and D and E and F,                                        -- 0
        B and C,                                                                -- 1
        A and B and G and E and D,                                              -- 2
        A and B and G and C and D,                                              -- 3
        F and G and B and C,                                                    -- 4
        A and F and G and C and D,                                              -- 5
        A and F and G and C and D and E,                                        -- 6
        A and B and C,                                                          -- 7
        A and B and C and D and E and F and G,                                  -- 8
        A and B and G and F and C and D,                                        -- 9
        A and B and G and F and E and C,                                        -- A
        F and G and C and D and E,                                              -- B (b)
        A and F and E and D,                                                    -- C
        B and G and E and D and C,                                              -- D (d)
        A and F and G and E and D,                                              -- E
        A and F and G and E                                                     -- F
    );
begin
end entity;

architecture seg7_encoderHex_rtl of seg7_encoderHex is
begin
    o_CathodeEnable <= c_SegmentData(to_integer(unsigned(i_HexDigit)));
end architecture;

-----------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

library common;
use common.Common.ALL;

entity seg7_timing is
    generic (
        g_DigitNum : natural;                                                   -- number of digits the display has

        g_ClkFreq       : real;                                                 -- i_Clk frequency (Hz)
        g_RefreshRate   : real                                                  -- frequency at which the whole display is
                                                                                --   refreshed (Hz)
    );
    port (
        i_Clk   : in std_logic;
        i_Rst   : in std_logic;

        o_SelectedAnodeOneHot  : out std_logic_vector((g_DigitNum-1) downto 0); -- selected anode index, converted to one-hot encoding
                                                                                -- each anode index is represented by its own bit
        o_SelectedAnodeIdx     : out std_logic_vector(
                Common_F_GetWidth(g_DigitNum-1) - 1 downto 0                    -- see further comment lines on why g_DigitNum-1
            )
    );
begin
end entity;

-- the problem here is how to hot encode a single value?
-- in binary, even a bit has two values (0 and 1), so by that
-- notion, to hot encode a single bit of information, you need two bits
--
-- this becomes problematic when you drive a single digit, since this resolves out
-- to two anode bits
--
-- the actual problem when driving a single digit is not one-hot encoding, but the
-- fact that the whole refresh unit is unneeded - we'll fix that using
-- generate statements

architecture seg7_timing_rtl of seg7_timing is
    constant c_MaxSelectedAnodeIdxVal   : natural := g_DigitNum-1;              -- 0 is one of the states
    constant c_SelectedAnodeIdxWidth    : natural := Common_F_GetWidth(c_MaxSelectedAnodeIdxVal);

    signal SelectedAnodeIdx : std_logic_vector (c_SelectedAnodeIdxWidth-1 downto 0);

    constant c_DigitPrescaleFactor : natural := Common_F_GetTicks_Freq(
            g_ClkFreq, g_RefreshRate * real(g_DigitNum)                         -- the prescaler sets the time of a single segment
        );                                                                      -- enables the anode index counter for one tick

    signal NextDigit : std_logic;
begin

    DigitPrescaler:
    entity common.timing_prescaler(timing_prescaler_rtl)
        generic map(
            g_PrescaleFactor => c_DigitPrescaleFactor
        )
        port map(
            i_Clk => i_Clk,
            i_Rst => i_Rst,

            o_Enable => NextDigit
        );

    AnodeCounter:
    entity common.timing_counter(timing_counter_rtl)
        generic map(
            g_MaxValue => c_MaxSelectedAnodeIdxVal
        )
        port map(
            i_Clk => i_Clk,
            i_Rst => i_Rst,
            i_Enable => NextDigit,

            o_CounterValue => SelectedAnodeIdx,
            o_MaxAchieved => open
        );

    DecoderToOneHot:
    entity common.jellybean_decoder(jellybean_decoder_rtl)
        generic map(
            g_InWidth => SelectedAnodeIdx'length
        )
        port map(
            i_InVec => SelectedAnodeIdx,
            o_OutVec => o_SelectedAnodeOneHot
        );

    -- we also output the index, for the purposes of the vector to hex mux
    o_SelectedAnodeIdx <= SelectedAnodeIdx;

end architecture;

-----------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library common;
use common.Common.ALL;

-- entity that combines all of the above modules to allow for "plug & play" usage
-- 7-segment displays
entity seg7_hexController is
    generic(
        g_VecWidth : natural;                                                   -- vector size

        g_ClkFreq       : real;                                                 -- i_Clk frequency (Hz)
        g_RefreshRate   : real := 60.0                                          -- frequency at which the whole display is
                                                                                --   refreshed (Hz)
    );
    port(
        i_Clk   : in std_logic;
        i_Rst   : in std_logic;

        i_Vec   : in std_logic_vector(g_VecWidth-1 downto 0);

        o_AnodeEnable   : out std_logic_vector((g_VecWidth / 4)-1 downto 0);    -- one-hot encoded
        o_CathodeEnable : out std_logic_vector(7 downto 0)
    );
begin
end entity;

architecture seg7_hexController_rtl of seg7_hexController is
    constant c_DigitNum                 : natural := g_VecWidth / 4;
    constant c_MaxSelectedAnodeIdxVal   : natural := c_DigitNum-1;              -- 0 is one of the states

    signal SelectedAnodeIdx : std_logic_vector(Common_F_GetWidth(c_MaxSelectedAnodeIdxVal)-1 downto 0);

    signal HexDigit : std_logic_vector(3 downto 0);
begin
    Timing:
    entity common.seg7_timing(seg7_timing_rtl)
        generic map(
            g_DigitNum => c_DigitNum,
            g_ClkFreq => g_ClkFreq,
            g_RefreshRate => g_RefreshRate
        )
        port map(
            i_Clk => i_Clk,
            i_Rst => i_Rst,

            o_SelectedAnodeOneHot => o_AnodeEnable,
            o_SelectedAnodeIdx => SelectedAnodeIdx
        );

    HexDigitMux:
    entity common.jellybean_mux(jellybean_mux_rtl)
        generic map(
            g_InWidth => i_Vec'length,
            g_OutWidth => HexDigit'length
        )
        port map(
            i_InVec => i_Vec,
            i_Select => SelectedAnodeIdx,

            o_OutVec => HexDigit
        );

    HexEncoder:
    entity common.seg7_encoderHex(seg7_encoderHex_rtl)
        port map(
            i_HexDigit => HexDigit,

            o_CathodeEnable => o_CathodeEnable
        );
end architecture;
