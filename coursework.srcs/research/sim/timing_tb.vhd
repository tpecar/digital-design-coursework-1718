-----------------------------------------------------------------------------------------------
-- The coding convention used is based on the
--  [1] Design and VHDL handbook for VLSI development - CNES Edition
--      available @ https://github.com/VHDLTool/VHDL_Handbook_CNE/releases
--
-- and in cases where the aforementioned specification misses rules, we ammend it with
--  [2] ALSE's VHDL Design Rules & Coding Style
--      available @ https://wiki.electroniciens.cnrs.fr/images/VHDL_Coding_eng.pdf
--
-- Information that might be ambiguous or missing from the above lists is mentioned inline
-- using --##
-----------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library common;
use common.Common.ALL;

entity timing_tb is
    generic (
        g_MaxValue : natural := 4;
        g_PrescalerMax : natural := 3
    );
begin
end entity;

--## architectures meant for simulation get the _sim postfix
architecture timing_tb_sim of timing_tb is
    constant c_ClockPeriod : time := 10 us;

    signal Clk : std_logic;
    signal Rst : std_logic;

    -- counter specific signals
    signal CounterValue : std_logic_vector (Common_F_GetWidth(g_MaxValue)-1 downto 0);

    -- prescaler specific signals
    signal PrescalerEnable : std_logic;
begin
    --## component names are without prefixes
    Counter:
    entity common.timing_counter(timing_counter_rtl)
        generic map (
            g_MaxValue => g_MaxValue
        )
        port map (
            i_Clk => Clk,
            i_Rst => Rst,
            i_Enable => '1',

            o_CounterValue => CounterValue,
            o_MaxAchieved => open
        );

    Prescaler:
    entity common.timing_prescaler(timing_prescaler_rtl)
        generic map (
            g_PrescaleFactor => g_PrescalerMax
        )
        port map (
            i_Clk => Clk,
            i_Rst => Rst,
            o_Enable => PrescalerEnable
        );

    -- clock generation
    P_Clk:
    process is
    begin
        Clk <= '0';
        loop
            wait for c_ClockPeriod/2;
            Clk <= not Clk;
        end loop;
        wait;
    end process;

    -- reset assertion
    P_Reset:
    process is
    begin
        Rst <= '1';
        wait for c_ClockPeriod*1;
        Rst <= '0';
        wait;
    end process;

    -- output state display/check
    --## process names use camel case without underscores (contrary to [1])
    P_CheckCounterState:
    process(Clk) is
    begin
        if rising_edge(Clk) then
            -- the second string has to be explicitly defined as such, otherwise the compiler
            -- tries to implicitly convert it to a std_logic_vector, which causes multiple definition clashes
            void := s("CounterValue: [") * CounterValue * s(" = 0x") * TOHEX * CounterValue * s("] ") > STDOUT;
        end if;
    end process;

    P_CheckPrescalerState:
    process(Clk) is
    begin
        if rising_edge(Clk) then
            if PrescalerEnable = '1' then
                void := s("Prescaler enable at counter value ") * CounterValue * s(" = 0x") * TOHEX * CounterValue * s("] ") > STDOUT;
            end if;
        end if;
    end process;
end architecture;
