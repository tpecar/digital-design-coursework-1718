-----------------------------------------------------------------------------------------------
-- The coding convention used is based on the
--  [1] Design and VHDL handbook for VLSI development - CNES Edition
--      available @ https://github.com/VHDLTool/VHDL_Handbook_CNE/releases
--
-- and in cases where the aforementioned specification misses rules, we ammend it with
--  [2] ALSE's VHDL Design Rules & Coding Style
--      available @ https://wiki.electroniciens.cnrs.fr/images/VHDL_Coding_eng.pdf
--
-- Information that might be ambiguous or missing from the above lists is mentioned inline
-- using --##
-----------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library common;
use common.Common.ALL;

entity seg7_tb is
    generic (
        g_MaxValue : natural := 255
    );
begin
end entity;

architecture seg7_tb_sim of seg7_tb is
    constant c_ClockPeriod : time := 10 us;

    signal Clk : std_logic;
    signal Rst : std_logic;

    -- counter specific signals
    signal CounterValue : std_logic_vector (
             Common_F_GetWidth(g_MaxValue)-1 downto 0
        );
    signal CounterMaxAchieved : std_logic;

    -- segment display specific signals
    signal Seg7AnodeEnable : std_logic_vector((CounterValue'length / 4)-1 downto 0);
    signal Seg7CathodeEnable : std_logic_vector(7 downto 0);

    -- seg7_timing_rtl test
--    constant c_DigitNum : natural := 4;
--    signal SelectedAnodeOneHot  : std_logic_vector((c_DigitNum - 1) downto 0);
--    signal SelectedAnodeIdx     : std_logic_vector(
--                Common_F_GetWidth(c_DigitNum-1) - 1 downto 0
--            );
begin
    --## component names are without prefixes
    Counter:
    entity common.timing_counter(timing_counter_rtl)
        generic map (
            g_MaxValue => g_MaxValue
        )
        port map (
            i_Clk => Clk,
            i_Rst => Rst,
            i_Enable => '1',

            o_CounterValue => CounterValue,
            o_MaxAchieved => CounterMaxAchieved
        );

    Seg7Controller:
    entity common.seg7_hexController(seg7_hexController_rtl)
        generic map(
            g_VecWidth => CounterValue'length,
            g_ClkFreq => 20.0,
            g_RefreshRate => 1.0
        )
        port map(
            i_Clk => Clk,
            i_Rst => Rst,
            i_Vec => CounterValue,

            o_AnodeEnable => Seg7AnodeEnable,
            o_CathodeEnable => Seg7CathodeEnable
        );

    -- separate test for seg7_timing_rtl
--    Seg7Timing:
--    entity common.seg7_timing(seg7_timing_rtl)
--        generic map (
--            g_DigitNum  => c_DigitNum,
--
--            g_ClkFreq   => 20.0,
--            g_RefreshRate => 1.0
--        )
--        port map (
--            i_Clk => Clk,
--            i_Rst => Rst,
--
--            o_SelectedAnodeOneHot => SelectedAnodeOneHot,
--            o_SelectedAnodeIdx => SelectedAnodeIdx
--        );

    -- clock generation
    P_Clk:
    process is
    begin
        Clk <= '0';
        loop
            wait for c_ClockPeriod/2;
            Clk <= not Clk;
        end loop;
        wait;
    end process;

    -- reset assertion
    P_Reset:
    process is
    begin
        Rst <= '1';
        wait for c_ClockPeriod*1;
        Rst <= '0';
        wait;
    end process;
end architecture;
