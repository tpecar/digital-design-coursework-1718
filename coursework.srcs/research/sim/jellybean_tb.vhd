-----------------------------------------------------------------------------------------------
-- The coding convention used is based on the
--  [1] Design and VHDL handbook for VLSI development - CNES Edition
--      available @ https://github.com/VHDLTool/VHDL_Handbook_CNE/releases
--
-- and in cases where the aforementioned specification misses rules, we ammend it with
--  [2] ALSE's VHDL Design Rules & Coding Style
--      available @ https://wiki.electroniciens.cnrs.fr/images/VHDL_Coding_eng.pdf
--
-- Information that might be ambiguous or missing from the above lists is mentioned inline
-- using --##
-----------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library common;
use common.Common.ALL;
use common.timing_counter_cfg.ALL;

entity jellybean_tb is
    generic(
        g_MaxValue : natural := 3
    );
begin
end entity;

architecture jellybean_tb_sim of jellybean_tb is
    constant c_ClockPeriod : time := 10 us;

    signal Clk : std_logic;
    signal Rst : std_logic;

    -- counter specific
    constant timing_counter_cfg : T_timing_counter_cfg := F_timing_counter_cfg(MaxValue => g_MaxValue);

    signal CounterValue : std_logic_vector (timing_counter_cfg.CounterValue_length-1 downto 0);

    signal MuxOut : std_logic_vector (3 downto 0);

    signal OneHotOut : std_logic_vector(3 downto 0);
begin
    Counter:
    entity common.timing_counter(timing_counter_rtl)
        generic map (
            timing_counter_cfg => timing_counter_cfg
        )
        port map (
            i_Clk => Clk,
            i_Rst => Rst,
            i_Enable => '1',

            o_CounterValue => CounterValue,
            o_MaxAchieved => open
        );

    -- clock generation
    P_Clk:
    process is
    begin
        Clk <= '0';
        loop
            wait for c_ClockPeriod/2;
            Clk <= not Clk;
        end loop;
        wait;
    end process;

    -- reset assertion
    P_Reset:
    process is
    begin
        Rst <= '1';
        wait for c_ClockPeriod*1;
        Rst <= '0';
        wait;
    end process;

    -- units under test
    Mux:
    entity common.jellybean_mux(jellybean_mux_rtl)
        generic map(
            g_InWidth => 16,
            g_OutWidth => 4
        )
        port map(
            i_InVec => "0000" & "1111" & "0101" & "1010",
            i_Select => CounterValue,

            o_OutVec => MuxOut
        );

    Decoder:
    entity common.jellybean_decoder(jellybean_decoder_rtl)
        generic map(
            g_InWidth => 2
        )
        port map(
            i_InVec => CounterValue,

            o_OutVec => OneHotOut
        );
end architecture;
