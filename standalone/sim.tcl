# pretty print
proc blockprint {str} {
    puts "-------------------------------------------------------------------------------"
    puts $str
    puts "-------------------------------------------------------------------------------"
}
# normal initialization
proc init {} {
    set curr_wave [current_wave_config]
    if { [string length $curr_wave] == 0 } {
    if { [llength [get_objects]] > 0} {
        add_wave /
        set_property needs_save false [current_wave_config]
    } else {
        send_msg_id Add_Wave-1 WARNING "No top level signals found. Simulator will start without a wave window. If you want to open a wave window go to 'File->New Waveform Configuration' or type 'create_wave_config' in the TCL console."
    }
    }
    log_wave -r /

    run 100us
}
# design recompile
proc recompile {} {
    # prepare simulation executables
    # some tools write to stderr even during a successful run, so ignore that
    if {[catch {exec -ignorestderr ./prepare_sim.sh $::env(PRJ_FILE) $::env(TOP_DESIGN_UNIT)} result]} {
        blockprint $result
        puts "Failed to prepare simulation."
        return
    }
    blockprint $result
}
# design reload
proc reload {} {
    if { [string length [current_sim]] > 0 } {
        close_sim
        recompile
    }
    # load new simulation instance
    xsim $::env(SNAPSHOT)
    init
}
# start simulation
init
