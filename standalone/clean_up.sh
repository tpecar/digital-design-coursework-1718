#!/bin/bash -f
source common.sh

PREV_RUN_DIR="$1"
if [ ! -n "$PREV_RUN_DIR" ]
then
    echo "Usage: $0 PREV_RUN_DIR"
    exit 1
fi

RUN_TIME=$(date +%Y%m%d_%H%M%S_%N)

# clean up project from previous runs
if [ ! -d $PREV_RUN_DIR ]
then
    echo "[WARN] Creating ${PREV_RUN_DIR}"
    ExecStep mkdir ${PREV_RUN_DIR}
fi
echo "Moving files from previous runs.."
ExecStep mkdir "${PREV_RUN_DIR}/${RUN_TIME}"

for curFile in \
    $(
    find . -maxdepth 1 -type f      \
        \(                          \
               \( -name '*.log' \)  \
            -o \( -name '*.jou' \)  \
            -o \( -name '*.wdb' \)  \
            -o \( -name '*.pb'  \)  \
        \)                          \
        -a -print
    )
do
    curTarget="${PREV_RUN_DIR}/${RUN_TIME}/$(basename $curFile)"
    printf '%-40s -> %40s\n' "[$curFile]" "[$curTarget]"
    ExecStep mv "$curFile" "$curTarget"
done 
