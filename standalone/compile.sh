#!/bin/bash -f
source common.sh

PRJ_FILE="$1"
if [ ! -n "$PRJ_FILE" ]
then
    echo "Usage: $0 PRJ_FILE"
    exit 1
fi
# Additional options: -93_mode
ExecStep xvhdl -v 2 --incr --relax -prj $PRJ_FILE 2>&1 | tee -a compile.log
