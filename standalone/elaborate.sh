#!/bin/bash -f
source common.sh

SNAPSHOT="$1"
TOP_DESIGN_UNIT="$2"
if [ ! -n "$TOP_DESIGN_UNIT" ]
then
    echo "Usage: $0 SNAPSHOT TOP_DESIGN_UNIT"
    exit 1
fi

# disable webtalk tcl
if [ ! -d "./xsim.dir/$SNAPSHOT/webtalk" ]
then
    mkdir -p "./xsim.dir/$SNAPSHOT/webtalk"
fi

WEBTALK="./xsim.dir/$SNAPSHOT/webtalk/xsim_webtalk.tcl"
if [ -w $WEBTALK ]
then
    truncate -s 0 $WEBTALK
    chmod -w $WEBTALK
fi

# run elaboration
# Additional options: -93_mode
ExecStep xelab -v 2 --incr --debug typical --relax --mt auto -L common -L secureip --snapshot $SNAPSHOT $TOP_DESIGN_UNIT -log elaborate.log
