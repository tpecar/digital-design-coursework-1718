#!/bin/bash -f
source common.sh

SNAPSHOT=$1
TCL_BATCH=$2

if [ ! -n "$TCL_BATCH" ]
then
    echo "Usage: $0 SNAPSHOT TCL_BATCH"
    exit 1
fi
ExecStep xsim $SNAPSHOT -tclbatch $TCL_BATCH -log simulate.log -gui
