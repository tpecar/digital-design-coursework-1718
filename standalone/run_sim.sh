#!/bin/bash -f
source common.sh

export PRJ_FILE="$1"
export TOP_DESIGN_UNIT="$2"
if [ ! -n "$TOP_DESIGN_UNIT" ]
then
    echo "Usage: $0 PRJ_FILE TOP_DESIGN_UNIT"
    echo "TOP_DESIGN_UNIT is library.toplevel_entity"
    exit 1
fi

export SNAPSHOT="$(basename $PRJ_FILE | cut -d"." -f1)_behav"

# project run
ExecStep ./prepare_sim.sh "$PRJ_FILE" "$TOP_DESIGN_UNIT"
ExecStep ./simulate.sh "$SNAPSHOT" sim.tcl
