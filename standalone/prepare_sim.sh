#!/bin/bash -f
source common.sh

PRJ_FILE="$1"
TOP_DESIGN_UNIT="$2"
if [ ! -n "$TOP_DESIGN_UNIT" ]
then
    echo "Usage: $0 PRJ_FILE TOP_DESIGN_UNIT"
    echo "TOP_DESIGN_UNIT is library.toplevel_entity"
    exit 1
fi

SNAPSHOT="$(basename $PRJ_FILE | cut -d"." -f1)_behav"

# project run
ExecStep ./compile.sh "$PRJ_FILE"
ExecStep ./elaborate.sh "$SNAPSHOT" "$TOP_DESIGN_UNIT"
echo "Finished ok."
