# any intermediary error condition should finish the whole pipeline
set -o pipefail

# common functionality used by the scripts
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
    echo "$0: Failed @ [$@]"
    exit $RETVAL
fi
}
# source the needed environment
source /home/tpecar/faks/1semester/dn/orodja/vivado2017.3/Vivado/2017.3/settings64.sh
