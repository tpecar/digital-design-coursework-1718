# Welcome to my magical land of VHDL!
*Where simulators run free, compilers go crazy and synthesis tools break*

## What is this?
Magic, duh!

On a more serious note, this is a collection of stuff I've written during my course of digital design, and the corresponding voyage that ensued due to my curiosity.
Be sure to check the [wiki](https://bitbucket.org/tpecar/digital-design-coursework-1718/wiki/) for some more documentation, occasional rants and completely irrelevant hilarity.

## Disclaimer
This repository contains some indecent examples on how you can make more flexible designs, prototype faster and achieve better results (*not*) in this archaic digital design language of the ancients.
Some of the code is not for the faint of heart, so you better hide this from your local guru, professor, instructor and/or mom.

At the same time it should be obvious that this is not military grade code, so I suggest not to use it for something highly critical.
In other words, you might just as well set your system on fire and it would still be more reliable than if it used this macaroni art.

## As far as the license is concered
Essentially, this is provided as a learning material for anyone with a sadistic wish of learning VHDL.
In that notion, I am releasing all of the content on this repository and its wiki under MIT license.

If anything comes handy, great. Try not to get hurt while using it.
